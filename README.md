Orpheus Ka Sangeet

Link to the Website: https://www.doc.gold.ac.uk/~cgust001/creativeproject/orpheus/home.html

The Live Music Box is an interactive, user-dependent installation which takes in live webcam input and produces a desired output. It takes user input and on the basis of that, manipulates the outputted sound. The final version of our project requires the camera input from three different users.

With the help of the different instruments and their sounds available, the user would be able to choose an instrument based on their liking and together all three would be able to work together and do a band performance of sorts. The users will also be able to colour pick their specific virtual puppet, making it more fun and interactive for both the user and the audience both. We have implemented it in such a way that each user can use a different body part for every different instrument, or one user can play all the instruments at once by positioning the screens vertically upright.

In case of a live performance, we would like the project to be displayed in a separate room than where the participants are creating their videos. This is because we do not want the participants to feel shy and we want to make sure that they enjoy it to the fullest during this experience.

Our intended audience lies between the age group 10-60. This is because we want to make sure that the project is safe and self-explanatory for both the youngest and the oldest user. It is a demographic with some internet experience.

When the user clicks on the link provided to them, they are directed to a website which provides them with a brief description of what the project is about and also what is expected out of them. After that they can choose between the three instruments - piano, guitar and drums. We have used a background image that evokes a sense of movement and due to the presence of bright colours, it also provides the viewer with a sense of activeness.
