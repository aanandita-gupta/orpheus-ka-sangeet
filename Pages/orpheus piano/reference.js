// Webcam Piano TEMPLATE
var camera;
var prevImg;
var currImg;    
var diffImg;
var threshold;
var grid;
var formrand;
var torand;
var colorSlider;
var colorVal;

//Sounds
var pianoFreq = [130.81, 138.59, 146.83, 155.56, 164.81, 174.61, 185, 196, 207.65, 220, 233.08, 246.94, 261.63, 277.18, 293.66, 311.13, 329.63, 349.23, 369.99, 392, 415.30, 440, 466.16, 493.88, 523.25];
var color = [255,0,255,0,255,255,0,255,0,255,0,255,255,0,255,0,255,255,0,255,0,255,0,255,255];
var pianoSwitch = false;
var piano = new maximJs.maxiOsc();

function setup() {
    var canvas = createCanvas(windowWidth, windowHeight);
    canvas.parent('sketch'); //The sketch now belongs to the html DIV "sketch" and can so be positioned within the DOM
    pixelDensity(1);
    camera = createCapture(VIDEO);
    camera.hide();
    
    threshold = 0.070;
    grid = new Grid(640, 480);
    
    //Random (Index for colorArray) values for start and end of Color Lerp. Range from 0-192 as 192 is the max number of notes that can be printed to the screen
    fromrand = Math.floor(random(0,100));
    torand = Math.floor(random(101,192));
    
    //Slider to either choose the original random blue color(s) or color chosen by input. 
    //Total value of slider is 370, which is of course +10 than the values that are available by default for HSB but I find it easier to use a slider if the "off" setting has a bigger margin. And this is compensated for where the color is called.
    colorSlider = createSlider(0,370,0,10);
    colorSlider.position(650, 300);
    
    audioContext = new maximJs.maxiAudio();
    audioContext.play = playLoop;
    audioContext.outputIsArray(true, 2);
    audioInit = false;
    
}

function playLoop() {
    
    var sig = piano.sinewave(200);
    
    if(pianoSwitch == true)
    {
        this.output = sig * 0.5;
    }
    
}

function draw() {     
    background(120);
    image(camera, 0, 0);
    camera.loadPixels();
    
    if(!audioInit)
    {
         textAlign(CENTER);
         textSize(32);
         fill(255);
         text("Press any key to begin", width/2, height/2);
         return;
    }

    var smallW = camera.width/4;
    var smallH = camera.height/4;
    currImg = createImage(smallW, smallH);
    currImg.copy(camera, 0, 0, camera.width, camera.height, 0, 0, smallW, smallH); // save current frame
    currImg.filter(GRAY);
    currImg.filter(BLUR,3);
    diffImg = createImage(smallW,smallH);

    if (typeof prevImg !== 'undefined') 
    {
        currImg.loadPixels();
        prevImg.loadPixels();
        diffImg.loadPixels();
        
        for (var x = 0; x < currImg.width; x += 1) {
            for (var y = 0; y < currImg.height; y += 1) {
               // MAGIC HAPPENS HERE
                var index = (x + (y * currImg.width)) * 4;
                var redSource = currImg.pixels[index + 0];
                
                var rPrev = prevImg.pixels[index + 0];
                
                var d = Math.abs(redSource - rPrev);
                diffImg.pixels[index + 0] = d;
                diffImg.pixels[index + 1] = d;
                diffImg.pixels[index + 2] = d;
                diffImg.pixels[index + 3] = 255;
                
            }
        }
        diffImg.updatePixels();
    }
    prevImg = createImage(smallW,smallH);
    prevImg.copy(currImg, 0, 0, smallW, smallH, 0, 0, smallW, smallH);
    image(currImg, 640, 0);
    diffImg.filter(THRESHOLD,threshold);
    image(diffImg, 0, 0, camera.width, camera.height);
    grid.update(diffImg);
    
    //Slider instructions text
    textAlign(LEFT);
    textSize(15);
    text("Slide to:", 650, 265);
    text("Pick a Colour! (Almost) Any Colour!", 650, 280);
    textSize(10);
    text("Please note, every time you choose a new colour ", 650, 340);
    text("you will need to readjust the threshold level ", 650, 360);
    text("by clicking the left side of the large video.", 650, 380);
    
    console.log(pianoSwitch);
}

function mousePressed() 
{
    threshold = map(mouseX,0,640,0,1);
}

var Grid = function(_w, _h){
this.diffImg = 0;
this.noteWidth = 128;
this.noteHeight = 96;
this.worldWidth = _w;
this.worldHeight = _h;
this.numOfNotesX = int(this.worldWidth/this.noteWidth);
this.numOfNotesY = int(this.worldHeight/this.noteHeight);
this.arrayLength = this.numOfNotesX * this.numOfNotesY;
this.noteStates = [];
this.noteStates =  new Array(this.arrayLength).fill(0);
this.colorArray = [];
console.log(this);
console.log(_w, _h);

    
// Set colors of notes

for (var i=0;i<this.arrayLength;i++){

  colorMode(HSB);
  //Random, increasing values, so the colours of the notes will be different every time the page is refreshed. The hue will always return values between 200-240 since I only want shades of blue. As the aplha values are random, sometimes the more opaque values will be at the top, sometimes at the bottom and sometimes the from and to values will have (at least roughly) the same alpha value and at those times there is no discernable change in transparency.
  this.colorArray.push(color(random(200, 250), random(5 + 3 * i,100), random(5 + 3 * i, 100), random(0.2,0.9)));
}



this.update = function(_img){
  this.diffImg = _img;
  this.diffImg.loadPixels();
  for (var x = 0; x < this.diffImg.width; x += 1) {
      for (var y = 0; y < this.diffImg.height; y += 1) {
          var index = (x + (y * this.diffImg.width)) * 4;
          var state = diffImg.pixels[index + 0];
          if (state==255){
            var screenX = map(x, 0, this.diffImg.width, 0, this.worldWidth);
            var screenY = map(y, 0, this.diffImg.height, 0, this.worldHeight);
            var noteIndexX = int(screenX/this.noteWidth);
            var noteIndexY = int(screenY/this.noteHeight);
            var noteIndex = noteIndexX + noteIndexY*this.numOfNotesX;
            this.noteStates[noteIndex] = 1;
          }
      }
  }

  //this is what "ages" the notes so that as time goes by things can change.
  for (var i=0; i<this.arrayLength;i++){
    this.noteStates[i]-= 0.05;
    this.noteStates[i]=constrain(this.noteStates[i],0,1);
  }


  this.draw();
};

// this is where each note is drawn
// use can use the noteStates variable to affect the notes as time goes by
// after that region has been activated
this.draw = function(){
    
    //lerpColor
//    colorMode(HSB);
//    let from = color(this.colorArray[fromrand]); //Random index from first "half" of colorArray
//    let to = color(this.colorArray[torand]); //Random index from second "half" of colorArray
//    let interA = lerpColor(from, to, 0.33); //One third from from colour closer to to color
//    let interB = lerpColor(from, to, 0.66); //Two thirds from from colour closer to to color
    
  push();
  noStroke();
  for (var x=0; x<this.numOfNotesX; x++){
    for (var y=0; y<this.numOfNotesY; y++){
            var posX = this.noteWidth/2 + this.noteWidth;
            var posY = this.noteHeight/2 + this.noteHeight;
            var noteIndex = x + (y * this.numOfNotesX);
            if (this.noteStates[noteIndex]>0) {
            //Changes color according to where the pixel is on the video screen section(what quarter, heightwise (y-pos)) with Lerp Color
//            if(colorSlider.value() <= 10) //Accounts for value of colorSlider so user can choose this random blue output
                fill(random(100,200));
//            {
//              if(posY < camera.height/4)
//              {
//                fill(255,0,0);
//              }
//              else if(posY >= camera.height/4 && posY <= camera.height/2)
//              {
//                  fill(0,255,0);
//              }
//              else if(posY > camera.height/2 && posY < camera.height - camera.height/4)
//              {
//                  fill(255,125,0);
//              }
//              else if(posY >= camera.height - camera.height/4)
//              {
//                  fill(0,0,255);
//                  pianoSwitch = true;
//              }
//            }
//            else //If the user has activated colorSlider the colour output on the shapes will be determined by the value of the slider (0-360, since it is HSB values). 
//            {
//                fill(colorSlider.value() - 10,100,100,0.7);
//            }
                
                //Right side of video draws ellipses
//                if(posX > camera.width/2)
//                {
//                    ellipse(posX,posY,this.noteWidth , this.noteWidth); 
//                }
//                //Left side of video draws rectangles(squares)
//                else if(posX < camera.width/2)
//                {
//                    //Offsetting x-pos of rects so that they do not overlap with the ellipses down the middle and offsetting the y-pos so that they do not go below the video 
                    rect(posX-this.noteWidth/2,posY - this.noteHeight/2.5,this.noteWidth*0.9,this.noteHeight*0.9);
//                }
            }
        }
  }
  pop();
}

};

function keyPressed()
{
    if(!audioInit)
    {
       audioContext.init();
       audioInit = true;
       triggerAudioEvents();
    }
    
}
