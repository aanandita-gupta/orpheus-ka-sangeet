/*Orpheus - Drums*/

/*Image and grid*/
var camera;
var prevImg;
var currImg;    
var diffImg;
var threshold;
var grid;

/*Color-cover slider - Change color of transparent rectangle covering the the video output*/
var colorCoverSlider;

/*Drum text - Booleans to trigger labels for the three different parts*/
var hihat = false;
var kickdrum = false;

/*Sound*/
var audioContext;
var audioInit;

var kickdrumSample;
var hihatSample;


function setup() {
    
    var canvas = createCanvas(windowWidth/2, windowHeight);
    canvas.parent('sketch'); /*Assigns the sketch to the html DIV "sketch" and can so be positioned within the DOM*/
    pixelDensity(1);
    camera = createCapture(VIDEO);
    camera.hide();
    background(255,255,255,0);
    
    /*Set default value of threshold, this usually works in most indoor environments using the built in webcam. The level can be adjusted by pressing on the camera output.*/
    threshold = 0.070;
    /*Set dimensions of grid*/
    grid = new Grid(640, 480);
    
    /*Slider that lets you choose what colour you would like your figure to be tinted with*/
    colorCoverSlider = createSlider(0,255,0,10);
    colorCoverSlider.position(windowWidth * 0.1,windowHeight * 0.7);
    
    /*Initating sounds and audio Context*/
    audioContext = new maximJs.maxiAudio();
    audioContext.play = audioLoop;
    audioInit = false;
    
    kickdrumSample = new maximJs.maxiSample();
    hihatSample = new maximJs.maxiSample();
    
}

/*Sounds - Audio context. Settings of sounds and triggering of them*/
function audioLoop() {
    
    var sig = 0;

    /*Only trigger the different drum parts if the relevant booleans are true (this will only happen when the user moves in the relevant part of the screen*/
    if(kickdrum == false)
    {
        kickdrumSample.trigger();
    }

    if(hihat == false)
    {
        hihatSample.trigger();
    }
    
    if(kickdrum == true)
    {
        sig += kickdrumSample.play();
    }
    if(hihat == true)
    {
        sig += hihatSample.play();
    }
    
    /*Output of sound*/
    this.output = sig * 0.2;
}

function draw() {   
    
    /*Take camera input and position the output*/
    image(camera, 0, 0);
    camera.loadPixels();
    
    var smallW = camera.width/4;
    var smallH = camera.height/4;
    currImg = createImage(smallW, smallH);
    currImg.copy(camera, 0, 0, camera.width, camera.height, 0, 0, smallW, smallH); /*Saving a copy of the current frame*/
    currImg.filter(GRAY);
    currImg.filter(BLUR,3);
    diffImg = createImage(smallW,smallH); /*This image will have the threshold filter, the white colour will show up where there was last movement using frame differencing.*/

    if (typeof prevImg !== 'undefined') 
    {
        currImg.loadPixels();
        prevImg.loadPixels();
        diffImg.loadPixels();
        
        /*Looping through all pixels of the currImg and if a pixel is brighter in the current image (there has been movement there), set the equivalent pixel in diffImg to white.*/
        for (var x = 0; x < currImg.width; x += 1) {
            for (var y = 0; y < currImg.height; y += 1) {
                var index = (x + (y * currImg.width)) * 4;
                var redSource = currImg.pixels[index + 0];
                
                var rPrev = prevImg.pixels[index + 0];
                
                var d = Math.abs(redSource - rPrev);
                diffImg.pixels[index + 0] = d;
                diffImg.pixels[index + 1] = d;
                diffImg.pixels[index + 2] = d;
                diffImg.pixels[index + 3] = 255;
                
            }
        }
        /*Update diffImg pixels accordingly.*/
        diffImg.updatePixels();
    }
    
    prevImg = createImage(smallW,smallH);
    prevImg.copy(currImg, 0, 0, smallW, smallH, 0, 0, smallW, smallH);
    image(currImg, 840, 0);
    diffImg.filter(THRESHOLD,threshold);
    image(diffImg, 0, 0, camera.width, camera.height);
    grid.update(diffImg); /*Update the grid, check if any of the instrument sections need to be triggered.*/
    
    /*Color-cover - the coloured transparent rectangle covering the video output to give the white part of the output a visible hint of colour*/
    colorMode(HSB); /*Set to HSB so that the shade can more easily modified by changing just one value (hue).*/
    fill(colorCoverSlider.value(),100,50,0.2);
    rect(0,0,camera.width,camera.height);
    noFill();
    colorMode(RGB);
    
    /*Drum text - To indicate to the user whichpart of the drum kit has been triggered.*/
    fill(255);
    
    if(hihat == true)
    {
        textSize(40);
        text('HI-HAT', camera.width/2, camera.height/4);
    }
    if(kickdrum == true)
    {
        textSize(40);
        text('KICK DRUM',  camera.width/2, camera.height - camera.height/4);
    }
    
        /*As per regulations the audio context has to be triggered by user interaction, until the user presses a key and audioInit is set to true this message will be displayed. The user can still just move around and check how the camera output is affected to get a hang of how to play the instrument.*/
    if(!audioInit)
    {
         textAlign(CENTER);
         textSize(32);
         fill(255);
         text("Press any key to begin", width/2, height/2);
         return;
    }
    
}

function mousePressed() 
{
    /*Set the threshold of the p5j.js THRESHOLD filter being used.*/
    threshold = map(mouseX,0,640,0,1);
}

/*Setup of grid being used to trigger different parts of the drum kit.*/
var Grid = function(_w, _h){
this.diffImg = 0;
this.worldWidth = _w;
this.worldHeight = _h;
this.noteWidth = this.worldWidth/4;
this.numOfNotesX = 4;
this.numOfNotesY = 4;
this.arrayLength = this.numOfNotesX * this.numOfNotesY; /*Set areas of grid in an array, 16 in total.*/
this.noteStates = [];
this.noteStates =  new Array(this.arrayLength).fill(0);
this.colorArray = [];

/*Update the pixels, see if any section has been triggered. */
this.update = function(_img){
  this.diffImg = _img;
  this.diffImg.loadPixels();
  for (var x = 0; x < this.diffImg.width; x += 1) {
      for (var y = 0; y < this.diffImg.height; y += 1) {
          var index = (x + (y * this.diffImg.width)) * 4;
          var state = diffImg.pixels[index + 0];
          /*If any pixel is white within an area of the grid then activate that areas drum section.*/
          if (state==255){
            var screenX = map(x, 0, this.diffImg.width, 0, this.worldWidth);
            var screenY = map(y, 0, this.diffImg.height, 0, this.worldHeight);
            var noteIndexX = int(screenX/this.noteWidth);
            var noteIndexY = int(screenY/this.noteWidth);
            var noteIndex = noteIndexX + noteIndexY*this.numOfNotesX;
            this.noteStates[noteIndex] = 1;
          }
      }
  }

  /*Ages the sections so that they do not stay triggered forever*/
  for (var i=0; i<this.arrayLength;i++){
    this.noteStates[i]-= 0.05;
    this.noteStates[i]=constrain(this.noteStates[i],0,1);
  }


  this.draw();
};


this.draw = function(){
    
  push();
  noStroke();
    /*Loop through each section of grid and if they are active (value of this.noteStates is greater than 0), then draw the outline of the drum sections when they have been triggered, and setting their corresponding booleans to true so as to trigger the sound of it.*/
  for (var x=0; x<this.numOfNotesX; x++){
    for (var y=0; y<this.numOfNotesY; y++){
            var posX = this.noteWidth/2 + x*this.noteWidth;
            var posY = this.noteWidth/2 + y*this.noteWidth;
            var noteIndex = x + (y * this.numOfNotesX);
                
        /*Set boolean to true and trigger that specific drum part if that section of the grid is active (has not aged to 0) and it is above the center of the sketch.*/
                /*HI-HAT indication*/
                if(this.noteStates[0]>0 || this.noteStates[1]>0 || this.noteStates[4]>0 || this.noteStates[5]>0 || this.noteStates[2]>0 || this.noteStates[3]>0 || this.noteStates[6]>0 || this.noteStates[7]>0 && posY < camera.height/2)
                {
                    stroke(255);
                    fill(255);
                    hihat = true;
                    noFill();
                    rect(1,1,this.worldWidth, this.worldHeight/2-1);
                }
                else
                {
                    hihat = false;
                }
                /*Set boolean to true and trigger that specific drum part if that section of the grid is active (has not aged to 0) and it is below the center of the sketch.*/
                /*KICK DRUM indication*/
                if(this.noteStates[8]>0 || this.noteStates[9]>0 || this.noteStates[10]>0 || this.noteStates[11]>0 || this.noteStates[12]>0 || this.noteStates[13]>0 || this.noteStates[14]>0 || this.noteStates[15]>0 && posY > camera.height/2)
                {
                    stroke(255);
                    fill(255);
                    kickdrum = true;
                    noFill();
                    rect(1,this.worldHeight/2,this.worldWidth-2, this.worldHeight/2-1);
                    noStroke();
                }
                else 
                {
                    kickdrum = false;
                    
                }    
        }
  }
  pop();
}

};

/*Once a key has been pressed the audio context is active and the user can start playing the instrument.*/
function keyPressed()
{
    if(!audioInit)
    {
       audioContext.init();
       audioInit = true;
       audioContext.loadSample("assets/kickdrum.wav", kickdrumSample);
       audioContext.loadSample("assets/hihat.wav", hihatSample);
    }
    
}
