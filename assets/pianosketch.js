/*Orpheus - Piano*/
var camera;
var prevImg;
var currImg;    
var diffImg;
var threshold;
var grid;

/*Color-cover slider - Change color of transparent rectangle covering the the video output*/
var colorCoverSlider;

/*Sound*/
var audioContext;
var audioInit;

/*Piano Keys*/

//row 1
var C3 = false;
var C3Osc = new maximJs.maxiOsc();
var C3Freq = new maximEx.env();

var CSharp3 = false;
var CSharp3Osc = new maximJs.maxiOsc();
var CSharp3Freq = new maximEx.env();

var D3 = false;
var D3Osc = new maximJs.maxiOsc();
var D3Freq = new maximEx.env();

var DSharp3 = false;
var DSharp3Osc = new maximJs.maxiOsc();
var DSharp3Freq = new maximEx.env();

var E3 = false;
var E3Osc = new maximJs.maxiOsc();
var E3Freq = new maximEx.env();

var F3 = false;
var F3Osc = new maximJs.maxiOsc();
var F3Freq = new maximEx.env();

//row 2
var FSharp3 = false;
var FSharp3Osc = new maximJs.maxiOsc();
var FSharp3Freq = new maximEx.env();

var G3 = false;
var G3Osc = new maximJs.maxiOsc();
var G3Freq = new maximEx.env();

var GSharp3 = false;
var GSharp3Osc = new maximJs.maxiOsc();
var GSharp3Freq = new maximEx.env();

var A3 = false;
var A3Osc = new maximJs.maxiOsc();
var A3Freq = new maximEx.env();

//row 3
var ASharp3 = false;
var ASharp3Osc = new maximJs.maxiOsc();
var ASharp3Freq = new maximEx.env();

var B3 = false; 
var B3Osc = new maximJs.maxiOsc();
var B3Freq = new maximEx.env();

var C4 = false;
var C4Osc = new maximJs.maxiOsc();
var C4Freq = new maximEx.env();

var CSharp4 = false;
var CSharp4Osc = new maximJs.maxiOsc();
var CSharp4Freq = new maximEx.env();

var D4 = false;
var D4Osc = new maximJs.maxiOsc();
var D4Freq = new maximEx.env();

//row 4
var DSharp4 = false;
var DSharp4Osc = new maximJs.maxiOsc();
var DSharp4Freq = new maximEx.env();

var E4 = false;
var E4Osc = new maximJs.maxiOsc();
var E4Freq = new maximEx.env();

var F4 = false;
var F4Osc = new maximJs.maxiOsc();
var F4Freq = new maximEx.env();

var FSharp4 = false;
var FSharp4Osc = new maximJs.maxiOsc();
var FSharp4Freq = new maximEx.env();

var G4 = false;
var G4Osc = new maximJs.maxiOsc();
var G4Freq = new maximEx.env();

//row 5
var GSharp4 = false;
var GSharp4Osc = new maximJs.maxiOsc();
var GSharp4Freq = new maximEx.env();

var A4 = false;
var A4Osc = new maximJs.maxiOsc();
var A4Freq = new maximEx.env();

var ASharp4 = false;
var ASharp4Osc = new maximJs.maxiOsc();
var ASharp4Freq = new maximEx.env();

var B4 = false;
var B4Osc = new maximJs.maxiOsc();
var B4Freq = new maximEx.env();

var C5 = false;
var C5Osc = new maximJs.maxiOsc();
var C5Freq = new maximEx.env();

/*Piano frequencies*/
var pianoFreq = [130, 138.59, 146.83, 155.56, 164.81, 174.61, 185, 196, 207.65, 220, 233.08, 246.94, 261.63, 277.18, 293.66, 311.13, 329.63, 349.23, 369.99, 392, 415.30, 440, 466.16, 493.88, 523.25];

function setup() {
    
    var canvas = createCanvas(windowWidth/2, windowHeight);
    canvas.parent('sketch'); /*Assigns the sketch to the html DIV "sketch" and can so be positioned within the DOM*/
    pixelDensity(1);
    camera = createCapture(VIDEO);
    camera.hide();
    background(255,255,255,0);
    
    /*Set default value of threshold, this usually works in most indoor environments using the built in webcam. The level can be adjusted by pressing on the camera output.*/
    threshold = 0.070;
    /*Set dimensions of grid*/
    grid = new Grid(480, 480);
    
    //Slider that lets you choose what colour you would like your figure to be tinted with
    colorCoverSlider = createSlider(0,255,0,10);
    colorCoverSlider.position(windowWidth * 0.1,windowHeight * 0.9);
    
    /*Initiate settings for audio*/
    audioContext = new maximJs.maxiAudio();
    audioContext.play = audioLoop;
    audioInit = false;
     
}

function audioLoop() {

    var sampleOut = 0;
    
    /*Set how each key should be played. Sets the frequency and the attack/release of each key.*/
    
    /*First row*/
    if(C3 == true)
    {
        var a = C3Osc.sinewave(pianoFreq[0]) * C3Freq.ar(0.3, 0.1); 
    }
    if(CSharp3 == true)
    {
        var a = CSharp3Osc.sinewave(pianoFreq[1]) * CSharp3Freq.ar(0.3, 0.2);
    }
    if(D3 == true)
    {
        var a = D3Osc.sinewave(pianoFreq[2]) * D3Freq.ar(0.3, 0.2);
    }
    if(DSharp3 == true)
    {
        var a = DSharp3Osc.sinewave(pianoFreq[3]) * DSharp3Freq.ar(0.3, 0.2);
    }
    if(E3 == true)
    {
        var a = E3Osc.sinewave(pianoFreq[4]) * E3Freq.ar(0.3, 0.4);
    }
    
    /*Second row*/
    if(F3 == true)
    {
        var a = F3Osc.sinewave(pianoFreq[5]) * F3Freq.ar(0.3, 0.4);
    }
    if(FSharp3 == true)
    {
        var a = FSharp3Osc.sinewave(pianoFreq[6]) * FSharp3Freq.ar(0.3, 0.4);
    }
    if(G3 == true)
    {
        var a = G3Osc.sinewave(pianoFreq[7]) * G3Freq.ar(0.3, 0.2);
    }
    if(GSharp3 == true)
    {
        var a = GSharp3Osc.sinewave(pianoFreq[8]) * GSharp3Freq.ar(0.3, 0.3);
    }
    if(A3 == true)
    {
        var a = A3Osc.sinewave(pianoFreq[9]) * A3Freq.ar(0.3, 0.2);
    }
    
    /*Third row*/
    if(ASharp3 == true)
    {
        var a = ASharp3Osc.sinewave(pianoFreq[10]) * ASharp3Freq.ar(0.3, 0.2);
    }
    if(B3 == true)
    {
        var a = B3Osc.sinewave(pianoFreq[11]) * B3Freq.ar(0.3,0.2);
    }
    if(C4 == true)
    {
        var a = C4Osc.sinewave(pianoFreq[12]) * C4Freq.ar(0.3,0.2);
    }
    if(CSharp4 == true)
    {
        var a = CSharp4Osc.sinewave(pianoFreq[13]) * CSharp4Freq.ar(0.3,0.2);
    }
    if(D4 == true)
    {
        var a = D4Osc.sinewave(pianoFreq[14]) * D4Freq.ar(0.3, 0.3);
    }
    
    /*Fourth row*/
    if(DSharp4 == true)
    {
        var a = DSharp4Osc.sinewave(pianoFreq[15]) * DSharp4Freq.ar(0.3,0.2);
    }
    if(E4 == true)
    {
        var a = E4Osc.sinewave(pianoFreq[16]) * E4Freq.ar(0.3,0.3);
    }
    if(F4 == true)
    {
        var a = F4Osc.sinewave(pianoFreq[17]) * F4Freq.ar(0.3,0.3);
    }
    if(FSharp4 == true)
    {
        var a = FSharp4Osc.sinewave(pianoFreq[18]) * FSharp4Freq.ar(0.3, 0.3);
    }
    if(G4 == true)
    {
        var a = G4Osc.sinewave(pianoFreq[19]) * G4Freq.ar(0.3,0.3);
    }
//    /*Fifth row*/
    if(GSharp4 == true)
    {
       var a = GSharp4Osc.sinewave(pianoFreq[20]) * GSharp4Freq.ar(0.3,0.3);
    }
    if(A4 == true)
    {
        var a = A4Osc.sinewave(pianoFreq[21]) * A4Freq.ar(0.3,0.3);
    }
    if(ASharp4 == true)
    {
        var a = ASharp4Osc.sinewave(pianoFreq[22]) * ASharp4Freq.ar(0.3, 0.3);
    }
    if(B4 == true)
    {
        var a = B4Osc.sinewave(pianoFreq[23]) * B4Freq.ar(0.3, 0.3);
    }
    if(C5 == true)
    {
        var a = C5Osc.sinewave(pianoFreq[24]) * C5Freq.ar(0.3,0.3);
    }
    
    
    //triggering all the audio events by triggering the corresponding envelope
    if(C3 == true)
    {
        sampleOut += a;
        C3Freq.trigger();
    }
    if(CSharp3 == true)
    {
        sampleOut += a;
        CSharp3Freq.trigger();
    }
    if(D3 == true)
    {
        sampleOut += a;
        D3Freq.trigger();
    }
    if(DSharp3 == true)
    {
        sampleOut += a;
        DSharp3Freq.trigger();
    }
    if(E3 == true)
    {
        sampleOut += a;
        E3Freq.trigger();
    }
    /*Second row*/
    if(F3 == true)
    {
        sampleOut += a;
        F3Freq.trigger();
    }
    if(FSharp3 == true)
    {
        sampleOut += a;
        FSharp3Freq.trigger();
    }
    if(G3 == true)
    {
        sampleOut += a;
        G3Freq.trigger();
    }
    if(GSharp3 == true)
    {
        sampleOut += a;
        GSharp3Freq.trigger();
    }
    if(A3 == true)
    {
        sampleOut += a;
        A3Freq.trigger();
    }
    /*Third row*/
    if(ASharp3 == true)
    {
        sampleOut += a;
        ASharp3Freq.trigger();
    }
    if(B3 == true)
    {
        sampleOut += a;
        B3Freq.trigger();
    }
    if(C4 == true)
    {
        sampleOut += a;
        C4Freq.trigger();
    }
    if(CSharp4 == true)
    {
        sampleOut += a;
        CSharp4Freq.trigger();
    }
    if(D4 == true)
    {
        sampleOut += a;
        D4Freq.trigger();
    }
    /*Fourth row*/
    if(DSharp4 == true)
    {
        sampleOut += a;
        DSharp4Freq.trigger();
    }
    if(E4 == true)
    {
        sampleOut += a;
        E4Freq.trigger();
    }
    if(F4 == true)
    {
        sampleOut += a;
        F4Freq.trigger();
    }
    if(FSharp4 == true)
    {
        sampleOut += a;
        FSharp4Freq.trigger();
    }
    if(G4 == true)
    {
        sampleOut += a;
        G4Freq.trigger();
    }
    /*Fifth row*/
    if(GSharp4 == true)
    {
        sampleOut += a;
        GSharp4Freq.trigger();
    }
    if(A4 == true)
    {
        sampleOut += a;
        A4Freq.trigger();
    }
    if(ASharp4 == true)
    {
        sampleOut += a;
        ASharp4Freq.trigger();
    }
    if(B4 == true)
    {
        sampleOut += a;
        B4Freq.trigger();
    }
    if(C5 == true)
    {
        sampleOut += a;
        C5Freq.trigger();
    }
    
    this.output = sampleOut * 0.2;
    
}

function draw() {     
    
    image(camera, 0, 0);
    camera.loadPixels();
    camera.width = 480;

    var smallW = camera.width/4;
    var smallH = camera.height/4;
    currImg = createImage(smallW, smallH);
    currImg.copy(camera, 0, 0, camera.width, camera.height, 0, 0, smallW, smallH); /*Saving a copy of the current frame*/
    currImg.filter(GRAY);
    currImg.filter(BLUR,3);
    diffImg = createImage(smallW,smallH); /*This image will have the threshold filter, the white colour will show up where there was last movement using frame differencing.*/

    if (typeof prevImg !== 'undefined') 
    {
        currImg.loadPixels();
        prevImg.loadPixels();
        diffImg.loadPixels();
        
        /*Looping through all pixels of the currImg and if a pixel is brighter in the current image (there has been movement there), set the equivalent pixel in diffImg to white.*/
        for (var x = 0; x < currImg.width; x += 1) {
            for (var y = 0; y < currImg.height; y += 1) {
                var index = (x + (y * currImg.width)) * 4;
                var redSource = currImg.pixels[index + 0];
                
                var rPrev = prevImg.pixels[index + 0];
                
                var d = Math.abs(redSource - rPrev);
                diffImg.pixels[index + 0] = d;
                diffImg.pixels[index + 1] = d;
                diffImg.pixels[index + 2] = d;
                diffImg.pixels[index + 3] = 255;
                
            }
        }
        /*Update diffImg pixels accordingly.*/
        diffImg.updatePixels();
    }
    prevImg = createImage(smallW,smallH);
    prevImg.copy(currImg, 0, 0, smallW, smallH, 0, 0, smallW, smallH);
    image(currImg, 840, 0);
    diffImg.filter(THRESHOLD,threshold);
    image(diffImg, 0, 0, camera.width, camera.height);
    grid.update(diffImg); /*Update the grid, check if any of the instrument sections need to be triggered.*/
    
    /*Color-cover - the coloured transparent rectangle covering the video output to give the white part of the output a visible hint of colour*/
    colorMode(HSB); /*Set to HSB so that the shade can more easily modified by changing just one value (hue).*/
    fill(colorCoverSlider.value(),100,50,0.2);
    rect(0,0,camera.width,camera.height);
    noFill();
    colorMode(RGB);
    
    /*Key text - Actual text to display when key is triggered.*/
    textSize(30);
    fill(255);
    stroke(255);
    
    /*First row*/
    if(C3 == true)
    {
        text('C', 72, 85);
    }
    if(CSharp3 == true)
    {
        text('C#', 160, 85);
    }
    if(D3 == true)
    {
        text('D', 245, 85);
    }
    if(DSharp3 == true)
    {
        text('D#', 330, 85);
    }
    if(E3 == true)
    {
        text('E', 412, 85);
    }
    /*Second row*/
    if(F3 == true)
    {
        text('F', 72, 170);
    }
    if(FSharp3 == true)
    {
        text('F#', 160, 170);
    }
    if(G3 == true)
    {
        text('G', 245, 170);
    }
    if(GSharp3 == true)
    {
        text('G#', 330, 170);
    }
    if(A3 == true)
    {
        text('A', 412, 170);
    }
    /*Third row*/
    if(ASharp3 == true)
    {
        text('A#', 72, 255);
    }
    if(B3 == true)
    {
        text('B', 160, 255);
    }
    if(C4 == true)
    {
        text('C', 245, 255);
    }
    if(CSharp4 == true)
    {
        text('C#', 330, 255);
    }
    if(D4 == true)
    {
        text('D', 412, 255);
    }
    /*Fourth row*/
    if(DSharp4 == true)
    {
        text('D#', 72, 340);
    }
    if(E4 == true)
    {
        text('E', 160, 340);
    }
    if(F4 == true)
    {
        text('F', 245, 340);
    }
    if(FSharp4 == true)
    {
        text('F#', 330, 340);
    }
    if(G4 == true)
    {
        text('G', 412, 340);
    }
    /*Fifth row*/
    if(GSharp4 == true)
    {
        text('G#', 72, 425);
    }
    if(A4 == true)
    {
        text('A', 160, 425);
    }
    if(ASharp4 == true)
    {
        text('A#', 245, 425);
    }
    if(B4 == true)
    {
        text('B', 330, 425);
    }
    if(C5 == true)
    {
        text('C', 412, 425);
    }
    
    /*As per regulations the audio context has to be triggered by user interaction, until the user presses a key and audioInit is set to true this message will be displayed. The user can still just move around and check how the camera output is affected to get a hang of how to play the instrument.*/ 
    if(!audioInit)
    {
         textAlign(CENTER);
         textSize(32);
         fill(255);
         text("Press any key to begin", width/3, height/2);
         return;
    }
    
    noStroke();
    noFill();
    
}

function mousePressed() 
{
    /*Set the threshold of the p5j.js THRESHOLD filter being used.*/
    threshold = map(mouseX,0,640,0,1);
}

/*Setup of grid being used to trigger different parts of the drum kit.*/
var Grid = function(_w, _h)
{
    this.diffImg = 0;
    this.noteWidth = 85;
    this.worldWidth = _w;
    this.worldHeight = _h;
    this.numOfNotesX = int(this.worldWidth/this.noteWidth);
    this.numOfNotesY = int(this.worldHeight/this.noteWidth);
    this.arrayLength = this.numOfNotesX * this.numOfNotesY;
    this.noteStates = [];
    this.noteStates =  new Array(this.arrayLength).fill(0);
        
    /*Update the pixels, see if any section has been triggered. */
    this.update = function(_img)
    {
        this.diffImg = _img;
        this.diffImg.loadPixels();
        for (var x = 0; x < this.diffImg.width; x += 1) 
        {
            for (var y = 0; y < this.diffImg.height; y += 1)
            {
                var index = (x + (y * this.diffImg.width)) * 4;
                var state = diffImg.pixels[index + 0];
                /*If any pixel is white within an area of the grid then activate that areas drum section.*/
                if (state==255)
                {
                    var screenX = map(x, 0, this.diffImg.width, 0, this.worldWidth);
                    var screenY = map(y, 0, this.diffImg.height, 0, this.worldHeight);
                    var noteIndexX = int(screenX/this.noteWidth);
                    var noteIndexY = int(screenY/this.noteWidth);
                    var noteIndex = noteIndexX + noteIndexY*this.numOfNotesX;
                    this.noteStates[noteIndex] = 1;
                }
            }
        }
        for (var i=0; i<this.arrayLength;i++)
        {
            this.noteStates[i]-= 0.05;
            this.noteStates[i]=constrain(this.noteStates[i],0,1);
        }

        this.draw();
    }  


// use can use the noteStates variable to affect the notes as time goes by
// after that region has been activated
    this.draw = function()
    {
        push();
        noStroke();
        /*Loop through each section of grid and if they are active (value of this.noteStates is greater than 0), then draw the outline of the piano keys when they have been triggered, and setting their corresponding booleans to true so as to trigger the sound of it.*/
        for (var x=0; x<this.numOfNotesX; x++)
        {
            for (var y=0; y<this.numOfNotesY; y++)
            {
                var posX = this.noteWidth/2 + x * this.noteWidth; 
                var posY = this.noteWidth/2 + y * this.noteWidth;
                var noteIndex = x + (y * this.numOfNotesX);
                if (this.noteStates[noteIndex]>0)
                {
                    stroke(255);
                    noFill();
                    rect(posX,posY,this.noteWidth * 0.7,this.noteWidth * 0.7);
                }
                if(this.noteStates[0])
                {
                    C3 = true;
                }
                else
                {
                    C3 = false;
                }
                if(this.noteStates[1])
                {
                    CSharp3 = true;
                }
                else 
                {
                    CSharp3 = false;
                }
                if(this.noteStates[2])
                {
                    D3 = true;
                }
                else 
                {
                    D3 = false;
                }
                if(this.noteStates[3])
                {
                    DSharp3 = true;
                }
                else 
                {
                    DSharp3 = false;
                }
                if(this.noteStates[4])
                {
                    E3 = true;
                }
                else 
                {
                    E3 = false;
                }
                if(this.noteStates[5])
                {
                    F3 = true;
                }
                else 
                {
                    F3 = false;
                }
                if(this.noteStates[6])
                {
                    FSharp3= true;
                }
                else 
                {
                    FSharp3 = false;
                }
                if(this.noteStates[7])
                {
                    G3 = true;
                }
                else 
                {
                    G3 = false;
                }
                if(this.noteStates[8])
                {
                    GSharp3 = true;
                }
                else 
                {
                    GSharp3 = false;
                }
                if(this.noteStates[9])
                {
                    A3 = true;
                }
                else 
                {
                    A3 = false;
                }
                if(this.noteStates[10])
                {
                    ASharp3 = true;
                }
                else 
                {
                    ASharp3 = false;
                }
                if(this.noteStates[11])
                {
                    B3 = true;
                }
                else 
                {
                    B3 = false;
                }
                if(this.noteStates[12])
                {
                    C4 = true;
                }
                else 
                {
                    C4 = false;
                }
                if(this.noteStates[13])
                {
                    CSharp4 = true;
                }
                else 
                {
                    CSharp4 = false;
                }
                if(this.noteStates[14])
                {
                    D4 = true;
                }
                else 
                {
                    D4 = false;
                }
                if(this.noteStates[15])
                {
                    DSharp4 = true;
                }
                else 
                {
                    DSharp4 = false;
                }
                if(this.noteStates[16])
                {
                    E4 = true;
                }
                else 
                {
                    E4 = false;
                }
                if(this.noteStates[17])
                {
                    F4 = true;
                }
                else 
                {
                    F4 = false;
                }
                if(this.noteStates[18])
                {
                    FSharp4 = true;
                }
                else 
                {
                    FSharp4 = false;
                }
                if(this.noteStates[19])
                {
                    G4 = true;
                }
                else 
                {
                    G4 = false;
                }
                if(this.noteStates[20])
                {
                    GSharp4 = true;
                }
                else 
                {
                    GSharp4 = false;
                }
                if(this.noteStates[21])
                {
                    A4 = true;
                }
                else 
                {
                    A4 = false;
                }
                if(this.noteStates[22])
                {
                    ASharp4 = true;
                }
                else 
                {
                    ASharp4 = false;
                }
                if(this.noteStates[23])
                {
                    B4 = true;
                }
                else 
                {
                    B4 = false;
                }
                if(this.noteStates[24])
                {
                    C5 = true;
                }
                else 
                {
                    C5 = false;
                }
            }
            pop();
            
        }
    }
    
};

/*Once a key has been pressed the audio context is active and the user can start playing the instrument.*/
function keyPressed()
{
    if(!audioInit)
    {
       audioContext.init();
       audioInit = true;
    }
    
}